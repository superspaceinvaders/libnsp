# Nanosatellite Protocol Library

Welcome to the wiki of the Nanosatellite Protocol Library (libnsp).

The libnsp is a lightweight protocol stack handling the decoding and encoding of the Nanosatellite Protocol (NSP) messages used by e.g. Sinclair Interplanetary products, such as the ST-16 star trackers and the RW3 reaction wheels. Note that NSP is as well used by other companies, but to this date libnsp has been implemented towards the NSP specification found in the ST-16 and RW3 Interface Control Documents available on the public domain: [sinclairinterplanetary.com](www.sinclairinterplanetary.com)

The current implementation is written for C++11 compatibility, but with portability to C in mind i.e. only a limited set of C++ features are used beyond simple classes.

## License

Libnsp is provided under the BEER-WARE license (see LICENSE.md). It is a permissive no-nonsense license that shall be no brainer for the legal department.

## Support

Libnsp is provided without support. Feel free to contact the author for any support or development quotation: <robin.lilja@gmail.com>

## Warranty

There is no warranty. But libnsp comes with a nice set of unit tests.

## Build and test

Build libnsp by:

cd build  
cmake ..  
make

It is by default compiled as a static library.

Run the unit tests by:

./runtest

## Q&A

Q: Why is libnsp under such permissive license?  
A: Libnsp is not rocket science. Let's keep the licensing the same.

Q: If I want to pay for a beer or two, but can't wait until I meet the author, how do I do?  
A: Contributions are gladly accepted through PayPal: [paypal.me/rblilja](paypal.me/rblilja), or IBAN can be provided by sending an e-mail to: <robin.lilja@gmail.com>

Note: The author will pay tax in accordance to Swedish law as money is traded for a deed (libnsp), even if the contribution is volontary.  

Q: What sort of beer does the author prefer?  
A: IPA.

Q: How much time did you spend on developing libnsp?  
A: More or less a weekend, and some nights getting the unit testing up and running.

# Heritage

If you fly libnsp, the author would very much appreciate a short notification containing the mission name, or simply count++, to be sent to: <robin.lilja@gmail.com>
