/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @file nsp.h
 * @author Robin Lilja
 * @date 15 Nov 2018
 */

#ifndef _NSP_H_
#define _NSP_H_

#include <stdint.h>

#include "libnsp/slip.h"

#define NSP_MIN_LEN   5           ///< Minimum message length 3 bytes header plus 2 bytes CRC, no data
#define NSP_MAX_LEN   1033        ///< Maximum message length (TBC)

#define NSP_PF_BIT    0x80        ///< "Poll/Final" bit (0b10000000)
#define NSP_B_BIT     0x40        ///< "B" bit (0b01000000)
#define NSP_ACK_BIT   0x20        ///< "ACK" bit (0b00100000)
#define NSP_CMD_CODE  0x1F        ///< Command code (0b00011111)

#define NSP_CRC_POLY  0x8408      ///< 0x1021 reversed for bit order
#define NSP_CRC_INIT  0xFFFF      ///< Initialization value
#define NSP_CRC_OK    0x0000      ///< CRC OK

/**
 * NSP message header.
 */
typedef struct NSP_Header {

  /**
   * Empty constructor.
   */
  NSP_Header() {}

  /**
   * Constructor.
   * @param dst destination address.
   * @param src source address.
   * @param mcf message control field.
   */
  NSP_Header(unsigned char dst, unsigned char src, unsigned char mcf) : m_dst(dst), m_src(src), m_mcf(mcf) {}

  bool pfbit() { return NSP_PF_BIT & m_mcf; }                 ///< @return poll/final bit
  bool bbit() { return NSP_B_BIT & m_mcf; }                   ///< @return B bit
  bool ackbit() { return NSP_ACK_BIT & m_mcf; }               ///< @return ACK bit
  unsigned char cmdcode() { return NSP_CMD_CODE & m_mcf; }    ///< @return command code

  unsigned char m_dst;  ///< Destination address
  unsigned char m_src;  ///< Source address
  unsigned char m_mcf;  ///< Message Control Field
} nsp_header_t;

/**
 * NSP error status.
 */
//typedef enum class NSP_ERROR : unsigned char {
typedef enum NSP_ERROR {

	NO_ERROR     = 0x00, ///< Message is valid
	MIN_LEN_NOK,         ///< Message is too short
	CRC_NOK,             ///< CRC fault
} nsp_error_t;

/**
 * Nanosatellite Protocol decoding/encoding namespace.
 */
namespace NSP {

/**
 * Encode NSP frame.
 * @param hdr header.
 * @param frm frame byte buffer (destination). User needs to guarantee sufficient length.
 * @param dat data field byte buffer (source).
 * @param datlen length.
 * @return Length of frame.
 */
uint16_t encode(const nsp_header_t * hdr, unsigned char * frm, const unsigned char * dat, uint16_t datlen);

/**
 * Decode NSP frame.
 * @param hdr header.
 * @param dat data byte buffer (destination). User needs to guarantee sufficient length.
 * @param frm frame byte buffer (source).
 * @param frmlen frame length.
 * @param err message error status.
 * @return Number of data bytes retrieved.
 */
uint16_t decode(nsp_header_t * hdr, unsigned char * dat, const unsigned char * frm, uint16_t frmlen, nsp_error_t * err);

/**
 * Decode NSP frame from an already decoded SLIP frame.
 * @param hdr header.
 * @param dat data byte buffer (destination). User needs to guarantee sufficient length.
 * @param frm frame byte buffer (source).
 * @param frmlen frame length.
 * @param err message error status.
 * @return Number of data bytes retrieved.
 */
uint16_t decodeNoSLIP(nsp_header_t * hdr, unsigned char * dat, const unsigned char * frm, uint16_t frmlen, nsp_error_t * err);

/**
 * Get frame from provided FIFO byte buffer.
 * @param frm frame byte buffer (destination). User needs to guarantee sufficient length.
 * @param fifo fifo byte buffer (source).
 * @param fifonum number of bytes available in fifo.
 * @param sync set to true for a synchronous sampling system where the fifo is expected to contain a single complete frame.
 * @return Frame length, zero if no frame found.
 */
uint16_t getFrame(unsigned char * frm, const unsigned char * fifo, uint16_t fifonum, bool sync);

}

#endif /* _NSP_H_ */
