/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @file nsp_common.h
 * @author Robin Lilja
 * @date 15 Nov 2018
 */

#ifndef _NSP_COMMON_H_
#define _NSP_COMMON_H_

#define NSP_OBC_DEFAULT_ADR 0x11  ///< Default OBC adress

#define NSP_PING 0x00
#define NSP_INIT 0x01
#define NSP_WRITE_FILE 0x08

#endif /* _NSP_COMMON_H_ */
