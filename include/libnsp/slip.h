/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @see RFC 1055
 *
 * @file slip.h
 * @author Robin Lilja
 * @date 29 Sep 2015
 */

#ifndef _SLIP_H_
#define _SLIP_H_

#include <string.h>		// Using memcpy()
#include <stdint.h>

/**
 * SLIP special characters.
 */
typedef enum SLIP_CHAR {

	FEND 	= 0xC0,    ///< Frame End
	FESC 	= 0xDB,    ///< Frame Escape
	TFEND = 0xDC,	   ///< Transposed Frame End
	TFESC = 0xDD 	   ///< Transposed Frame Escape
} slip_char_t;

/**
 * Serial Line Internet Protocol decoding/encoding namespace.
 */
namespace SLIP {

/**
 * The Decoder class encapsulates the state of an ongoing SLIP decoding process.
 * Specifically, this allows for streaming in input from a device driver and
 * handling frames as soon as the last byte has been received.
 */
class Decoder {
public:
    /**
     * Constructs a Decoder to decode one SLIP frame.
     * @param destination the buffer into which the frame will be decoded.
     */
    Decoder(uint8_t * destination);

    /**
     * Accept a new byte of input and update the internal state.
     * This may write a new decoded byte to the target buffer.
     *
     * Initial sequences of FEND bytes are ignored.
     *
     * Do not call this method if the decoding is already done (@see isDone)
     *
     * @param input the new input byte
     */
    void acceptInput(const uint8_t input);

    /**
     * Checks if the decoding process is done. This can happen either if a FEND
     * byte has been received after a non-zero amount of data bytes or if a
     * decoding error occurred.
     *
     * @returns true if done, false otherwise
     */
    bool isDone() const {
        return state == DecodeState::DONE
            || state == DecodeState::ERROR;
    }

    /**
     * Checks if the decoding process ended due to an error (e.g. a FESC that
     * was not followed by either TFEND or TFESC).
     *
     * @returns true if error, false otherwise
     */
    bool inErrorState() const {
        return state == DecodeState::ERROR;
    }

    /**
     * If this gets called after decoding is done, it returns the length of the
     * decoded frame in the destination buffer.
     * @return the length of the decoded frame in bytes
     */
    size_t getLength() const { return length; }

    /**
     * If this gets called after decoding is done, it returns the destination
     * buffer which now contains the decoded SLIP frame.
     * @return the destination buffer
     */
    uint8_t * getFrame() const { return destination; }

    /**
     * Resets the decoder to a state in which it is able to accept input again.
     * The destination buffer address is not changed.
     */
    void reset() {
        length = 0;
        state = DecodeState::CONTINUE;
    }

private:
    enum class DecodeState {
        CONTINUE, ESCAPE, DONE, ERROR
    };

    uint8_t * destination;
    size_t length;
    DecodeState state;
};

/**
 * Encode given packet according to SLIP.
 * @param frm frame byte buffer (destination).
 * @param msg message byte buffer (source).
 * @param msglen message length.
 * @return Frame length.
 */
uint16_t encode(unsigned char * frm, const unsigned char * msg, uint16_t msglen);

/**
 * Decode given frame according to SLIP.
 * @param msg message byte buffer (destination).
 * @param frm frame byte buffer (source).
 * @param frmlen frame length.
 * @return Packet length.
 */
uint16_t decode(unsigned char * msg, const unsigned char * frm, uint16_t frmlen);

/**
 * Get frame from FIFO buffer.
 * @param frm frame byte buffer (destination).
 * @param fifo fifo byte buffer (source).
 * @param fifonum number of bytes available in fifo.
 * @return Frame length, zero if no frame found.
 */
uint16_t getFrame(unsigned char * frm, const unsigned char * fifo, uint16_t fifonum);

/**
 * Get necessary frame (destination) buffer length.
 * @param msg message byte buffer (source).
 * @param msglen message length.
 * @return Frame length.
 */
uint16_t getFrameLength(const unsigned char * msg, uint16_t msglen);

}

#endif /* _SLIP_H_ */
