/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @file encode.cpp
 * @author Robin Lilja
 * @date 20 Nov 2018
 */

#include "gtest/gtest.h"

#include "libnsp/nsp.h"
#include "libnsp/nsp_common.h"

TEST(Encode, ZeroData) {

  nsp_header_t hdr{ 0x06, 0x11, (NSP_PF_BIT | NSP_ACK_BIT | (NSP_CMD_CODE & NSP_PING)) };

  // Reference PING frame
  unsigned char ref[] = {0xc0, 0x06, 0x11, 0xa0, 0xa9, 0xc6, 0xc0};
  uint16_t reflen = sizeof(ref);

  // Dummy data, sets zero length in encode call below
  unsigned char dat[] = {0xde, 0xad, 0xbe, 0xef};

  unsigned char frm[256];

  uint16_t frmlen = NSP::encode(&hdr, frm, dat, 0);

  // Assert equal frame length
  ASSERT_EQ(reflen, frmlen);

  // Assert header
  ASSERT_EQ(hdr.m_dst, 0x06);
  ASSERT_EQ(hdr.m_src, 0x11);
  ASSERT_EQ(hdr.pfbit(), true);
  ASSERT_EQ(hdr.bbit(), false);
  ASSERT_EQ(hdr.ackbit(), true);
  ASSERT_EQ(hdr.cmdcode(), NSP_PING);

  // Assert identical frames
  for (uint16_t i = 0; i < reflen; i++) {

    SCOPED_TRACE(i);
    ASSERT_EQ(ref[i], frm[i]);
  }
}

TEST(Encode, NoEscape) {

  nsp_header_t hdr{ 0x06, 0x11, (NSP_PF_BIT | NSP_ACK_BIT | (NSP_CMD_CODE & NSP_INIT)) };

  // Reference INIT frame
  unsigned char ref[] = {0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0};
  uint16_t reflen = sizeof(ref);

  unsigned char dat[] = {0x00, 0x20, 0x00, 0x00};
  uint16_t datlen = sizeof(dat);

  unsigned char frm[256];

  uint16_t frmlen = NSP::encode(&hdr, frm, dat, datlen);

  // Assert equal frame lengths
  ASSERT_EQ(reflen, frmlen);

  // Assert header
  ASSERT_EQ(hdr.m_dst, 0x06);
  ASSERT_EQ(hdr.m_src, 0x11);
  ASSERT_EQ(hdr.pfbit(), true);
  ASSERT_EQ(hdr.bbit(), false);
  ASSERT_EQ(hdr.ackbit(), true);
  ASSERT_EQ(hdr.cmdcode(), NSP_INIT);

  // Assert identical frames
  for (uint16_t i = 0; i < reflen; i++) {

    SCOPED_TRACE(i);
    ASSERT_EQ(ref[i], frm[i]);
  }
}

TEST(Encode, Escape) {

  nsp_header_t hdr{ 0x06, 0x11, (NSP_PF_BIT | NSP_ACK_BIT | (NSP_CMD_CODE & NSP_WRITE_FILE)) };

  // Reference WRITE FILE frame
  unsigned char ref[] = {0xc0, 0x06, 0x11, 0xa8, 0x00, 0x1c, 0xdb, 0xdc, 0xdb, 0xdd, 0xdc, 0xdd, 0x41, 0xae, 0xc0};
  uint16_t reflen = sizeof(ref);

  unsigned char dat[] = {0x00, 0x1c, 0xc0, 0xdb, 0xdc, 0xdd};
  uint16_t datlen = sizeof(dat);

  unsigned char frm[256];

  uint16_t frmlen = NSP::encode(&hdr, frm, dat, datlen);

  // Assert equal frame lengths
  ASSERT_EQ(reflen, frmlen);

  // Assert header
  ASSERT_EQ(hdr.m_dst, 0x06);
  ASSERT_EQ(hdr.m_src, 0x11);
  ASSERT_EQ(hdr.pfbit(), true);
  ASSERT_EQ(hdr.bbit(), false);
  ASSERT_EQ(hdr.ackbit(), true);
  ASSERT_EQ(hdr.cmdcode(), NSP_WRITE_FILE);

  // Assert identical frames
  for (uint16_t i = 0; i < reflen; i++) {

    SCOPED_TRACE(i);
    ASSERT_EQ(ref[i], frm[i]);
  }
}
