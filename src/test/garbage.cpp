/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @file garbage.cpp
 * @author Robin Lilja
 * @date 20 Nov 2018
 */

#include "gtest/gtest.h"

#include "libnsp/nsp.h"
#include "libnsp/nsp_common.h"

TEST(Garbage, Spurious) {

  // Reference INIT frame (without garabage)
  unsigned char ref[] = {0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0};
  uint16_t reflen = sizeof(ref);

  // FIFO with preceding and trailing garbage
  unsigned char fifo[] = {0xde, 0xad, 0xbe, 0xef, 0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0, 0xde, 0xad, 0xbe, 0xef};
  uint16_t fifonum = sizeof(fifo);

  unsigned char frm[256];

  uint16_t frmlen = NSP::getFrame(frm, fifo, fifonum, true);

  // Assert equal frame lengths
  ASSERT_EQ(reflen, frmlen);

  // Assert identical frames
  for (uint16_t i = 0; i < reflen; i++) {

    SCOPED_TRACE(i);
    ASSERT_EQ(ref[i], frm[i]);
  }
}

TEST(Garbage, SpuriousDefeat) {

  nsp_header_t hdr;
  nsp_error_t err;

  // FIFO with preceding garbage containing FEND
  unsigned char fifo[] = {0xde, 0xad, 0xc0, 0xbe, 0xef, 0xde, 0xad, 0xbe, 0xef, 0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0};
  uint16_t fifonum = sizeof(fifo);

  unsigned char frm[256];

  // Will return faulty frame (note that SLIP will drop the true FEND, is this how it should work..?)
  uint16_t frmlen = NSP::getFrame(frm, fifo, fifonum, true);

  unsigned char dat[256];

  // Decode faulty frame
  NSP::decode(&hdr, dat, frm, frmlen, &err);

  // Assert CRC fail
  ASSERT_EQ(err, NSP_ERROR::CRC_NOK);
}

TEST(Garbage, Relic) {

  // Reference INIT frame (without garabage)
  unsigned char ref[] = {0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0};
  uint16_t reflen = sizeof(ref);

  // FIFO with preceding incomplete relic frame
  unsigned char fifo[] = {0xde, 0xad, 0xbe, 0xef, 0xc0, 0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0};
  uint16_t fifonum = sizeof(fifo);

  unsigned char frm[256];

  uint16_t frmlen = NSP::getFrame(frm, fifo, fifonum, true);

  // Assert equal frame lengths
  ASSERT_EQ(reflen, frmlen);

  // Assert identical frames
  for (uint16_t i = 0; i < reflen; i++) {

    SCOPED_TRACE(i);
    ASSERT_EQ(ref[i], frm[i]);
  }
}

TEST(Garbage, Repeat) {

  // Reference INIT frame (without garabage)
  unsigned char ref[] = {0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0};
  uint16_t reflen = sizeof(ref);

  // FIFO with preceding and trailing FEND
  unsigned char fifo[] = {0xc0, 0xc0, 0xc0, 0x06, 0x11, 0xa1, 0x00, 0x20, 0x00, 0x00, 0x62, 0x8f, 0xc0, 0xc0, 0xc0};
  uint16_t fifonum = sizeof(fifo);

  unsigned char frm[256];

  uint16_t frmlen = NSP::getFrame(frm, fifo, fifonum, true);

  // Assert equal frame lengths
  ASSERT_EQ(reflen, frmlen);

  // Assert identical frames
  for (uint16_t i = 0; i < reflen; i++) {

    SCOPED_TRACE(i);
    ASSERT_EQ(ref[i], frm[i]);
  }
}
