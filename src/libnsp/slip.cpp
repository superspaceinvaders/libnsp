/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <robin.lilja@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return - Robin Lilja
 *
 * NSP Library - Nanosatellite Protocol Library
 *
 * @see RFC 1055
 *
 * @file slip.cpp
 * @author Robin Lilja
 * @date 29 Sep 2015
 */

#include "libnsp/slip.h"

namespace SLIP {

Decoder::Decoder(uint8_t * dst) :
    destination(dst), length(0), state(DecodeState::CONTINUE) {
}

void Decoder::acceptInput(const uint8_t b) {
    switch (state) {
    case DecodeState::ESCAPE:
        if (b == TFEND) {
            destination[length++] = FEND;
            state = DecodeState::CONTINUE;
        } else if (b == TFESC) {
            destination[length++] = FESC;
            state = DecodeState::CONTINUE;
        } else {
            state = DecodeState::ERROR;
        }
        break;
    case DecodeState::CONTINUE:
        if (b == FEND) {
            if (length != 0) {
                state = DecodeState::DONE;
            }
        } else if (b == FESC) {
            state = DecodeState::ESCAPE;
        } else {
            destination[length++] = b;
        }
        break;
    default:
        // either error or completed
        state = DecodeState::ERROR;
        break;
    }
}

uint16_t encode(unsigned char * frm, const unsigned char * pkg, uint16_t pkglen) {

	uint16_t j = 0;

	frm[j++] = FEND;	// Precede with FEND character

	// Iterate bytes of packet buffer
	for (uint16_t i = 0; i < pkglen; i++) {

		// Copy byte from source to destination
		frm[j] = pkg[i];

		// Got hit on FEND character
		if (pkg[i] == FEND) {

			frm[j] = FESC;
			frm[++j] = TFEND;
		}
		// Got hit on FESC character
		else if (pkg[i] == FESC) {

			frm[++j] = TFESC;
		}

		j++;
	}

	frm[j++] = FEND;	// Finally add trailing FEND character

	return j;
}

uint16_t decode(unsigned char * pkg, const unsigned char * frm, uint16_t frmlen) {
    Decoder decoder(pkg);
    for (uint16_t i = 0; i < frmlen && !decoder.isDone(); i++) {
        decoder.acceptInput(frm[i]);
    }
    if (!decoder.isDone() || decoder.inErrorState()) {
        return 0;
    }
    return decoder.getLength();
}

uint16_t getFrame(unsigned char * frm, const unsigned char * fifo, uint16_t fifonum) {

	// Frame search scenarios (perhaps not all..):

	// Correct frame
	//
	//		|<------memcpy-------->|
	//		|     |--search------->|
	//		[FEND][][][][][][][FEND][FEND][][]...

	// Incomplete frame (e.g. entire frame not yet received), nothing will be copied
	//
	//		      |--search--------->
	//		[FEND][][][][][][][][][]

	// Relic FEND causes false frame, frame of length two will be returned (higher layer needs to detect and reject)
	//
	//		|<-----memcpy------>|
	//		     |--search----->|
	//		[FEND]         [FEND][][][][][][][][][]

	// Erroneous frame missing starting FEND e.g. caused by above example (higher layer needs to reject if frame is faulty)
	//
	//		|<------memcpy-------->|
	//		| |-----search-------->|
	//		[][][][][][][][][][FEND][FEND][][][]...

	// Spurious bytes in-between frames will be interpreted as a frame (higher layer needs to detect and reject, typically done by CRC)
	//
	//		|<---------memcpy--------->|
	//		|    |-----search--------->|
	//		[FEND]...[spurious]...[FEND][FEND][][]...

	uint16_t frmlen = 0;

	// Iterate FIFO byte buffer for trainling FEND
	for ( uint16_t i = 1; i < fifonum; i++ ) { if ( fifo[i] == FEND ) { frmlen = i+1; break;} }

	// Copy frame from FIFO to given frame buffer
	if ( frmlen > 0 ) { memcpy(frm, fifo, frmlen); }

	return frmlen;
}

uint16_t getFrameLength(const unsigned char * msg, uint16_t msglen) {

	uint16_t count = 0;

	// Count the number of special characters in the message
	for (uint16_t i = 0; i < msglen; i++) { if ( (msg[i] == FEND) || (msg[i] == FESC)) { count++; } }

	return msglen+count+2;		// Add two for preceding and trailing FEND
}

}
